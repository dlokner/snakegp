from neural_network import NeuralNetwork
from global_params import *
from genetic_algorithm import Chromosome


def test_func(x):
    return x


def test_func_plus(x):
    return x + 1


net = NeuralNetwork(
    nodes_per_layer=[2, 2, 1],
    transfer_function=test_func,
    output_function=test_func,
    chromosome=Chromosome(np.arange(2*2+2*1+3)))

result = net.feed_forward(np.array([1, 1]))
print('result: ' + str(result) + ', 96 expected')

result = net.feed_forward(np.array([2, 3]))
print('result: ' + str(result) + ', 144 expected')

net = NeuralNetwork(
    nodes_per_layer=[2, 2, 1],
    transfer_function=test_func_plus,
    output_function=test_func,
    chromosome=Chromosome(np.arange(2*2+2*1+3)))

result = net.feed_forward(np.array([1, 1]))
print('result: ' + str(result) + ', 105 expected')
