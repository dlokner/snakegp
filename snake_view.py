import numpy as np
from global_params import *


def convert_zero_to_one(d: dict) -> dict:
    keys = d.keys()
    for k in keys:
        d[k] = 1 if d[k] == 0 else d[k]
    return d


# Search for specified value in ndarray.
# If value don't exists returns -1.
def find_value(array: np.ndarray, value: int) -> int:
    res = np.where(array == value)[0]
    if len(res) == 0:
        return -1
    else:
        return res[0]


# CREATES VIEW DICTIONARY
#
# expects snake view array ([0, 200, 0, 0])
def create_view(view: np.ndarray, scale: int) -> dict:
    wall = len(view) + 1

    snake = find_value(view, 100) + 1
    food = find_value(view, 200) + 1

    res = {'wall': wall / scale, 'snake': snake / scale, 'food': food / scale}
    res = convert_zero_to_one(res)
    return res


# Creates snake view on the LEFT from the head
def view_left(field: np.ndarray, head: tuple) -> dict:
    row = field[head[0]]
    row_view = row[:head[1]]
    reversed_row = row_view[::-1]
    return create_view(reversed_row, len(row))


# Creates snake view on the RIGHT from the head
def view_right(field: np.ndarray, head: tuple) -> dict:
    row = field[head[0]]
    row_view = row[head[1] + 1:]
    return create_view(row_view, len(row))


# Creates snake view to UP from the head
def view_up(field: np.ndarray, head: tuple) -> dict:
    rot_field = np.rot90(field)
    rot_head = (field[0].size - 1 - head[1], head[0])
    return view_left(rot_field, rot_head)


# Creates snake view to DOWN from the head
def view_down(field: np.ndarray, head: tuple) -> dict:
    rot_field = np.rot90(field)
    rot_head = (field[0].size - 1 - head[1], head[0])
    return view_right(rot_field, rot_head)


# Get view of snake considering current direction to be UP
def get_views(field: np.ndarray, head: tuple) -> dict:
    d = {}
    d.update(left=view_left(field, head))
    d.update(right=view_right(field, head))
    d.update(up=view_up(field, head))
    d.update(down=view_down(field, head))
    return d


# Rotates coordinate
def rotate_coordinate(row: int, col: int, coordinate: tuple, direction: int) -> tuple:
    if direction == DOWN:
        return row - (coordinate[0] + 1), col - (coordinate[1] + 1)
    elif direction == RIGHT:
        return col - (coordinate[1] + 1), coordinate[0]
    elif direction == LEFT:
        return coordinate[1], row - (coordinate[0] + 1)
    else:
        return coordinate


# Rotates matrix
def rotate_field(field: np.ndarray, direction: int) -> np.ndarray:
    if direction == DOWN:
        return np.rot90(field, 2)
    elif direction == RIGHT:
        return np.rot90(field, 1)
    elif direction == LEFT:
        return np.rot90(field, 3)
    else:
        return field


# Gets snake view considering current direction
def get_views_direction(field: np.ndarray, head: tuple, direction: int) -> dict:
    rotated_field = rotate_field(field, direction)

    shape = field.shape
    rotated_head = rotate_coordinate(shape[0], shape[1], head, direction)

    return get_views(rotated_field, rotated_head)


# creates dictionary with value 1 assigned to current direction
def get_direction_dictionary(direction: int) -> dict:
    d = {'left': 0, 'right': 0, 'up': 0, 'down': 0}

    if direction == DOWN:
        d['down'] = 1
    elif direction == RIGHT:
        d['right'] = 1
    elif direction == LEFT:
        d['left'] = 1
    else:
        d['up'] = 1

    return d
