import gym

from global_params import *
from genetic_algorithm import Population

from snake import Snake

env: SnakeEnv = gym.make('Snake-v0')

best_individuals = []


def play_population(population: Population, generation=None):
    for chromosome in population.individuals:
        snake = Snake(chromosome, env)
        snake.play_game()
    best = population.highest_fitness()
    best_individuals.append(best)
    print()
    print('==============================================')
    print("============   GENERATION {}   ===============".format(generation))
    print('==============================================')
    print('   Individuals ' + str(population.size()))
    print('   Best fitness ' + str(best.fitness))
    print(best.genes)
    print('==============================================')


def main():
    p = Population()

    print('selection: ')
    selection = input()
    print('crossover: ')
    crossover = input()
    print('mutation: ')
    mutation = input()

    p.create_new_population(POPULATION_SIZE, (10 * 16 + 3 * 10 + 10 + 3), GENE_BOUNDS)
    generation = 0
    while True:
        generation += 1

        print('...playing')
        play_population(p, generation)

        print('...generating new population')
        # evolve(population_size, probability_of_mutation, selection, crossover, mutation)

        p = p.evolve(p.size(), MUTATION_PROBABILITY, selection, crossover, mutation)


main()
