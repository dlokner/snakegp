import numpy as np
import random
import math

from typing import List

import global_params as gp


class Chromosome:
    def __init__(self, chromosome: np.ndarray = None, fitness=0.0):
        self.genes = chromosome
        self.fitness = fitness

    def create_chromosome(self, length: float, bounds: tuple):
        self.genes = np.random.uniform(bounds[0], bounds[1], length)
        return self

    def get_gene(self, index: int) -> int:
        return self.genes[index]

    def size(self):
        return 0 if self.genes is None else len(self.genes)

    def copy(self):
        chromosome_copy = self.genes.copy()
        return Chromosome(chromosome_copy)


class Population:
    def __init__(self, population: List[Chromosome] = None):
        if population is None:
            population = []
        self.individuals = population
        self.crossovers = {
            'alternately': crossover_alternately,
            'gauss': crossover_gauss,
            'arithmetic': crossover_arithmetic
        }
        self.mutations = {
            'gauss': mutate_chromosome_gauss_global_params,
            'random': mutate_chromosome_random
        }
        self.selections = {
            'roulette_wheel': self.roulette_wheel_selection,
            'k_tournament': self.k_tournament_selection,
            'elitism': self.elitism_selection
        }

    def copy(self):
        p = Population(self.individuals.copy())
        return p

    def size(self):
        return len(self.individuals)

    def add(self, chromosome: Chromosome):
        self.individuals.append(chromosome)

    def remove(self, chromosome: Chromosome):
        self.individuals.remove(chromosome)

    def get_individual(self, index: int) -> Chromosome:
        return self.individuals[index]

    def create_new_population(self, population_size: int,
                              chromosome_length: float, chromosome_bounds: tuple) -> List[Chromosome]:
        for _ in range(population_size):
            chromosome = Chromosome()
            self.add(chromosome.create_chromosome(chromosome_length, chromosome_bounds))
        return self.individuals

    def sort_population_by_fitness(self) -> List[Chromosome]:
        self.individuals.sort(key=lambda c: c.fitness, reverse=True)
        return self.individuals

    def roulette_wheel_selection(self, size: int) -> List[Chromosome]:
        # self.sort_population_by_fitness()
        selected = []
        fitness_sum = sum([c.fitness for c in self.individuals])
        for i in range(size):
            selected.append(roulette_wheel(self, fitness_sum))
        return selected

    def k_tournament_selection(self, size: int, k: int) -> List[Chromosome]:
        selected = []
        for i in range(size):
            selected.append(k_tournament(self, k))
        return selected

    def elitism_selection(self, size: int) -> List[Chromosome]:
        sorted_individuals = self.copy().sort_population_by_fitness()
        return sorted_individuals[:size]

    # currently unused
    def highest_fitness(self):
        highest: Chromosome or None = None

        for i in range(self.size()):
            if i == 0:
                highest = self.get_individual(i)
            else:
                current = self.get_individual(i)
                if current.fitness > highest.fitness:
                    highest = current
        return highest

    # evolves this population
    #
    # selection - roulette wheel
    # crossover - alternately
    # mutation - random
    def evolve1(self, size, mutation_probability):
        parents = self.roulette_wheel_selection(size * 2)
        children_arr = cross_parents(parents, crossover_alternately)
        children_population = Population(children_arr)
        children_population.mutate(mutation_probability, mutate_chromosome_random)
        return children_population

    # evolves this population
    #
    # selection - k tournament
    # crossover - gaussian
    # mutation - gaussian
    def evolve2(self, size, mutation_probability):
        parents = self.k_tournament_selection(size * 2, gp.K)

        children_arr = cross_parents(parents, crossover_gauss)
        children_population = Population(children_arr)

        children_population.mutate(mutation_probability, mutate_chromosome_gauss_global_params)
        return children_population

    def evolve(self, size: int, mutation_probability: float,
               select='roulette_wheel', crossover='arithmetic', mutation='gauss'):
        parents: List[Chromosome]
        if select == 'k_tournament':
            parents = self.k_tournament_selection(size * 2 - 4, gp.K)
            elite = self.elitism_selection(2)
            print('elite -> ' + str(elite[0].fitness))
        else:
            select_function = self.selections.get(select, self.roulette_wheel_selection)
            parents = select_function(size * 2 - 4)
            elite = self.elitism_selection(2)

        crossover_function = self.crossovers.get(crossover, crossover_arithmetic)
        children_arr = cross_parents(parents, crossover_function)
        children_population = Population(children_arr)

        mutation_function = self.mutations.get(mutation, mutate_chromosome_gauss_global_params)
        children_population.mutate(mutation_probability, mutation_function)
        children_population.add(elite[0])
        children_population.add(elite[1])
        return children_population

    def mutate(self, rate: float, mutation_function: callable):
        for i in range(self.size()):
            mutation_function(self.get_individual(i), rate)


# ================
#   SELECTION
# ================
def roulette_wheel(population: Population, fitness_sum: float) -> Chromosome:
    pick = random.uniform(0.0, fitness_sum)
    current = 0.0
    for chromosome in population.individuals:
        current += chromosome.fitness
        if current > pick:
            return chromosome


def k_tournament(population: Population, k: int) -> Chromosome:
    selected = []
    population_size = population.size()
    for i in range(k):
        index = random.randint(0, population_size-1)
        selected.append(population.get_individual(index))

    chosen = selected[0] if k > 0 else None

    for i in range(1, k):
        if selected[i].fitness > chosen.fitness:
            chosen = selected[i]

    return chosen


# ===============
#   CROSSOVER
# ===============
def crossover_alternately(c1: Chromosome, c2: Chromosome) -> Chromosome:
    if c1.size() != c2.size():
        raise ValueError('Chromosomes must have same number of genes.')
    child = []
    for i in range(0, c1.size(), 2):
        child.append(c1.get_gene(i))
        child.append(c2.get_gene(i+1))
    return Chromosome(np.array(child))


def crossover_arithmetic(c1: Chromosome, c2: Chromosome) -> Chromosome:
    if c1.size() != c2.size():
        raise ValueError('Chromosomes must have same number of genes.')
    return Chromosome((c1.genes + c2.genes) / 2)


def crossover_gauss(c1: Chromosome, c2: Chromosome) -> Chromosome:
    if c1.size() != c2.size():
        raise ValueError('Chromosomes must have same number of genes.')
    child = []
    primary = c1 if c1.fitness > c2.fitness else c2
    secondary = c2 if c2.fitness > c1.fitness else c1
    for i in range(0, c1.size()):
        p_gene = primary.get_gene(i)
        s_gene = secondary.get_gene(i)

        gaussian_random = random.gauss(p_gene, math.fabs(p_gene - s_gene)/3)
        gaussian_random = math.fabs(gaussian_random)
        gaussian_random = keep_in_bounds(gaussian_random, gp.GENE_BOUNDS[0], gp.GENE_BOUNDS[1])

        dif = math.fabs(gaussian_random - math.fabs(p_gene))
        dif = dif if s_gene > p_gene else - dif

        child.append(p_gene + dif)
    return Chromosome(np.array(child))


# cross parent chromosomes - calls cross_chromosomes for every single pair of chromosomes
def cross_parents(parents: List[Chromosome], cross_function: callable) -> List[Chromosome]:
    if len(parents) % 2 != 0:
        raise ValueError('Odd number of parents.')
    children = []
    for i in range(0, len(parents), 2):
        child = cross_function(parents[i], parents[i + 1])
        children.append(child)
    return children


# =============
#   MUTATION
# =============

# mutate chromosome - random value
def mutate_chromosome_random(c: Chromosome, mutation_rate: float) -> Chromosome:
    for i in range(c.size()):
        if np.random.choice([True, False], p=[mutation_rate, 1-mutation_rate]):
            c.genes[i] = random.uniform(gp.GENE_BOUNDS[0], gp.GENE_BOUNDS[1])
    return c


# mutate chromosome - adds value from interval given as argument -> (included, excluded)
def mutate_chromosome_gauss(c: Chromosome, mutation_rate: float, sigma: float, bounds: tuple) -> Chromosome:
    for i in range(c.size()):
        if np.random.choice([True, False], p=[mutation_rate, 1-mutation_rate]):
            value = c.genes[i]
            new_value = random.gauss(value, sigma)
            new_value = keep_in_bounds(new_value, bounds[0], bounds[1])
            c.genes[i] = new_value
    return c


def mutate_chromosome_gauss_global_params(c: Chromosome, mutation_rate: float) -> Chromosome:
    mutated_chromosome = mutate_chromosome_gauss(c, mutation_rate, gp.GAUSSIAN_MUTATION_SIGMA, gp.GENE_BOUNDS)
    return mutated_chromosome


# =============================================== #

def keep_in_bounds(x: float, lower: float, upper: float):
    x = lower if x < lower else x
    x = upper if x > upper else x
    return x
