import gym

import time as t

import snake_view as view
from neural_network import NeuralNetwork
from global_params import *


def _dict_to_list(d: dict) -> list:
    return list(d.values())


# extract values from view dictionary
def extract_views_list(views: dict) -> list:
    directions = views.values()
    res = []
    for d in directions:
        res += _dict_to_list(d)
    return res


# defines inputs of neural network (views and directions)
def define_inputs(obs: np.ndarray, act: int, head: tuple):
    views: dict = view.get_views_direction(obs, head, act)
    directions: dict = view.get_direction_dictionary(act)

    views_list = extract_views_list(views)
    directions_list = _dict_to_list(directions)

    return np.array(views_list + directions_list)


def fitness_function(time: int, reward: float, food: int) -> float:
    return time/1000 + 2**food


def calculate_action(previous_action: int, action: int) -> int:
    if previous_action == SnakeAction.UP:
        if action == 0:
            return SnakeAction.LEFT
        elif action == 1:
            return SnakeAction.RIGHT
        else:
            return SnakeAction.UP
    elif previous_action == SnakeAction.LEFT:
        if action == 0:
            return SnakeAction.DOWN
        elif action == 1:
            return SnakeAction.UP
        else:
            return SnakeAction.LEFT
    elif previous_action == SnakeAction.RIGHT:
        if action == 0:
            return SnakeAction.UP
        elif action == 1:
            return SnakeAction.DOWN
        else:
            return SnakeAction.RIGHT
    elif previous_action == SnakeAction.DOWN:
        if action == 0:
            return SnakeAction.RIGHT
        elif action == 1:
            return SnakeAction.LEFT
        else:
            return SnakeAction.DOWN   
    return -1


class Snake:
    def __init__(self, chromosome, env: SnakeEnv = None):
        self.chromosome = chromosome
        self.neural_net = NeuralNetwork(
            nodes_per_layer=[16, 10, 3],
            transfer_function=hyperbolic_tangent,
            output_function=softmax,
            chromosome=self.chromosome)

        if env:
            self.env = env
        else:
            self.env: SnakeEnv = gym.make('Snake-v0')

        self.snake = self.env.snake
        self.food_num = FOODS

        self.render = False

    def food_eaten(self):
        eaten = len(self.env.snake.body) - 1
        return eaten

    # snake plays game
    # returns snake reward
    def play_game(self) -> dict:
        self.env.set_foods(self.food_num)

        obs = self.env.reset()

        reward = 0.0
        time = 0
        timeout = 0
        act = INITIAL_ACTION
        head = self.snake.head

        while True:
            if RENDER:
                self.env.render()
                t.sleep(FRAME_TIME)

            time += 1

            inputs = define_inputs(obs, act, head)
            outputs = self.neural_net.feed_forward(inputs)

            previous_action = act
            act = np.argmax(outputs)
            act = calculate_action(previous_action, act)

            obs, rew, done, info = self.env.step(act)

            timeout += 1 if info['food_eaten'] == 0 else 0
            reward += rew

            done = True if (timeout >= SNAKE_TIMEOUT) else done

            if done:
                fitness = fitness_function(time, reward, self.food_eaten())
                self.chromosome.fitness = fitness
                return {
                    'time': time,
                    'reward': reward,
                    'food': self.food_eaten(),
                    'fitness': self.chromosome.fitness
                }
