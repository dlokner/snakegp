from snake_gym.envs.snake_env import SnakeAction, SnakeEnv
import numpy as np

UP = SnakeAction.UP
LEFT = SnakeAction.LEFT
RIGHT = SnakeAction.RIGHT
DOWN = SnakeAction.DOWN

SNAKE_TIMEOUT = 300
FOODS = 1
INITIAL_ACTION = UP

POPULATION_SIZE = 500
MUTATION_PROBABILITY = 0.1

RENDER = False
FRAME_TIME = 0.05

GENE_BOUNDS = (-3, 3)

GAUSSIAN_MUTATION_SIGMA = 0.1

K = 3


def softmax(x):
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()


def relu(x):
    return np.maximum(0, x)


def leaky_relu(x):
    return np.where(x > 0, x, x * 0.01)


def hyperbolic_tangent(x):
    e_x = np.exp(x)
    e_x_neg = np.exp(-x)
    return (e_x - e_x_neg) / (e_x + e_x_neg)


def sigmoid(x):
    e_x_neg = np.exp(-x)
    return 1 / (1 + e_x_neg)
