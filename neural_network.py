import numpy as np
from genetic_algorithm import Chromosome


def _create_split_array(array) -> list:
    res = []
    for i in range(0, len(array)):
        res.append(array[i] + (0 if i == 0 else res[i - 1]))
    return res


def _split_array_for_weights(array) -> list:
    res = []
    for a in array:
        res.append(a[0] * a[1])
    return res


class NeuralNetwork:

    def __init__(self,
                 nodes_per_layer,
                 transfer_function,
                 output_function,
                 chromosome: Chromosome):
        self.nodes_per_layer = nodes_per_layer
        self.transfer_function = transfer_function
        self.output_function = output_function

        self.chromosome_arr = chromosome.genes
        self.chromosome_len = self.__calculate_chromosome_length()
        self.chromosome = self.decode_chromosome(self.chromosome_arr)

        self.inputs = None
        self.output = None

        self.rand = np.random.RandomState()

    def feed_forward(self, inputs: np.ndarray):
        self.inputs = inputs
        self.check_inputs_length(inputs)

        i = -1

        for i in range(len(self.nodes_per_layer) - 2):
            output = self._feed(i, inputs, self.transfer_function)
            inputs = output

        output = self._feed(i + 1, inputs, self.output_function)
        self.output = output
        return output

    def _feed(self, layer, inputs, function):
        neurons = self.chromosome['w'][layer] * inputs
        offsets = self.chromosome['b'][layer]
        output = np.sum(neurons, axis=1) + offsets
        return function(output)

    # ==========================================

    def decode_chromosome(self, chromosome: np.ndarray):
        self.check_chromosome_length(chromosome)
        return self.__format_chromosome(chromosome, self.__chromosome_format_creator())

    def __chromosome_format_creator(self):
        w = []
        b = []

        for i in range(1, len(self.nodes_per_layer)):
            b.append(self.nodes_per_layer[i])
            w.append((self.nodes_per_layer[i], self.nodes_per_layer[i - 1]))

        return [w, b]

    def __format_chromosome(self, chromosome: np.ndarray, form: list):
        w_form = form[0]
        b_form = form[1]

        b_len = sum(b_form)
        w_len = self.chromosome_len - b_len

        b_split = _create_split_array(b_form)[0:-1]
        w_split = _split_array_for_weights(w_form)[0:-1]

        b = np.split(chromosome[w_len:], b_split)
        w = np.split(chromosome[:w_len], w_split)

        for i in range(0, len(w)):
            w[i] = np.reshape(w[i], w_form[i])

        return {'w': w, 'b': b}

    def __calculate_chromosome_length(self) -> int:
        length = 0
        for i in range(1, len(self.nodes_per_layer)):
            length += self.nodes_per_layer[i - 1] * self.nodes_per_layer[i]
            length += self.nodes_per_layer[i]
        return length

    def check_chromosome_length(self, chromosome: np.ndarray):
        if len(chromosome) != self.__calculate_chromosome_length():
            raise ValueError('Invalid number of genes in chromosome.')

    def check_inputs_length(self, inputs):
        if len(inputs) != self.nodes_per_layer[0]:
            raise ValueError('Invalid number of input arguments.')

    # def feed_forward(self, inputs: np.ndarray):
    #     self.inputs = inputs
    #     self.check_inputs_length(inputs)
    #
    #     i = 0
    #
    #     for i in range(len(self.nodes_per_layer) - 2):
    #         neurons = self.chromosome['w'][i] * inputs
    #         offsets = self.chromosome['b'][i]
    #         output = np.sum(neurons, axis=1) + offsets
    #         inputs = self.transfer_function(output)
    #
    #     neurons = self.chromosome['w'][i + 1] * inputs
    #     output = np.sum(neurons, axis=1) + self.chromosome['b'][i + 1]
    #     self.output = output
    #     return self.output_function(output)
